import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';

class Control {
    constructor(props) {
        this.type = 'test';
        this.name = '测试组件';
        this.key = Date.now();
        this.id = this.type + "_" + this.key;
        this.lock = false;
        this.dataType = 'string';

        this.props = {
            type: 'test',
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '测试组件',
            enName: '', // 英文名称
            inputType: 'text', //类型
            defaultValue: '',
            placeholder: '',
            required: false,
            ...props
        };
        this.rules = [
            {message: '必填字段', required: !!props?.required}
        ]
    }
}

Control.type = "test";
Control.label = "测试组件";
Control.icon = "https://t7.baidu.com/it/u=1819248061,230866778&fm=193&f=GIF";
export default {Control, Renderer, PropEditor, Viewer};
