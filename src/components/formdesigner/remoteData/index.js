export const remoteOption = {
    url: '',//请求地址
    method: 'GET',//接口请求方式：GET、POST
    headerList: [],//请求头参数
    paramList: [],//参数列表
    paramsType: 'form',//参数传递方式：form、json
    res: 'data',//数据层级，从接口返回的JSON对象中哪一层取数据，例如返回对象为：res.data.resultObj,则这里就填data.resultObj
}
