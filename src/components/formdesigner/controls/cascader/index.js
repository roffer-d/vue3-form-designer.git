import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("cascader", "级联选择");
        this.dataType = 'array';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '级联选择',
            enName: '',//英文名称
            defaultValue: [],
            placeholder: '请选择',
            required: false,
            requiredMessage: '必填字段',
            showAllLevels: true,
            disabled: false,
            clearable: true,
            filterable: true,
            valueField: 'label',
            expandTrigger: 'click',
            multiple: false,
            // checkStrictly: false,
            emitPath: true,
            showOptionLabel: false,
            remark: '',//描述信息
            options: [],
            dataType: 'static',//static，静态数据，remote，动态数据
            remoteOption: {
                ...remoteOption,
                labelKey: 'label',
                valueKey: 'value',
                childrenKey: 'children'
            }
        };
        this.rules = [{message: '必填字段', required: false}
            // ,{ type: "array", min: 1, message: '请选择接收人' }
        ];
    }
}

Control.type = "cascader";
Control.label = "级联选择";
export default {Control, Renderer, PropEditor, Viewer};
