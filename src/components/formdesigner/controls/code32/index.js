import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from '../input'
import Upload from '../upload'
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("code32", "32位码识别");
        let nameList = ['32位码信息']
        let controls = nameList.map(label => new Input.Control({label, required: true, disabled: true, lock: true}))
        let img = new Upload.Control({label: '图片', required: true, lock: true})
        controls.push(img)

        this.props = {
            type: 'code32',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '32位码识别',
            enName: '',//32位码识别
            remark: '',//描述信息
            columns: [
                {
                    label: '32位码信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "code32";
Control.label = '32位码识别';
export default {Control, Renderer, PropEditor, Viewer};
