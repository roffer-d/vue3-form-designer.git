import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
class Control extends BaseControl {
    constructor() {
        super("slider", "滑块");
        this.dataType='number';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '滑块',
            enName: '',//英文名称
            defaultValue: 0,
            disabled: false,
            required: false,
            requiredMessage: '必填字段',
            min: 0,
            max: 100,
            step: 1,
            showInput: false,
            showStops: false,
            showTooltip: true,
            remark: '',//描述信息
        };
    }
}
Control.type = "slider";
Control.label = "滑块";
export default { Control, Renderer, PropEditor,Viewer };
