import input from "./input";
import textarea from './textarea';
import inputnumber from './inputnumber';
import select from './select';
import radio from './radio';
import checkbox from './checkbox';
import color from './color';
import date from './date';
import time from './time';
import $switch from './switch';
import slider from './slider';
import text from './text';
import html from './html';
import link from './link';
import divider from './divider';
import upload from './upload';
import uploadImage from './uploadImage';
import editor from './editor';
import region from './region';
import cascader from './cascader';
import table from './table';
import tab from './tab';

import subForm from './subForm';
import position from './position';
import licenseDistinguish from './licenseDistinguish';
import businessLicenseDistinguish from './businessLicenseDistinguish';
import faceDistinguish from './faceDistinguish';
import rate from './rate';
import voiceToText from './voiceToText';
import licensePlateDistinguish from './licensePlateDistinguish';
import idCardDistinguish from './idCardDistinguish';
import currencyOcr from './currencyOcr';
import autograph from './autograph';
import geographicalPosition from './geographicalPosition';
import qrcode from './qrcode';
import code32 from './code32';
import txcode from './txcode';


export default {
    input,
    textarea,
    inputnumber,
    select,
    radio,
    checkbox,
    rate,
    color,
    date,
    time,
    'switch': $switch,
    slider,
    text,
    html,
    link,
    divider,

    upload,
    uploadImage,
    editor,
    region,
    cascader,
    table,
    tab,
    subForm,

    position,
    licenseDistinguish,
    businessLicenseDistinguish,
    faceDistinguish,

    voiceToText,
    licensePlateDistinguish,
    idCardDistinguish,
    currencyOcr,
    autograph,
    geographicalPosition,
    qrcode,
    code32,
    txcode
}

