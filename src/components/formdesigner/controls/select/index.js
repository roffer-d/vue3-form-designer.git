import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
class Control extends BaseControl {
    constructor(props) {
        super("select", "下拉选择",props?.lock || false);
        this.dataType='string';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '下拉选择',
            enName: '', // 英文名称
            inputType: 'text', //类型
            formatVerify: 'text', // 格式验证
            dictionary: '0', // 匹配字典
            defaultValue: '',
            placeholder: '请选择',
            required: false,
            requiredMessage: '必填字段',
            disabled: false,
            clearable: true,
            filterable: true,
            showOptionLabel: true,
            remark: '',//描述信息
            options: [
                { value: '值1', label: '选项1' },
                { value: '值2', label: '选项2' }
            ],
            ...props
        };
        this.rules = [{ message: '必填字段', required: false }];
    }
}
Control.type = "select";
Control.label = "下拉选择";
export default { Control, Renderer, PropEditor,Viewer };
