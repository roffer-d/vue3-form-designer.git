
import { randomWord } from "../utils";
class BaseControl {
    constructor(type, name,lock=false) {
        this.type = type;
        this.name = name;
        this.key = randomWord(false, 9);
        this.id = type + "_" + this.key;
        this.lock = lock;
    }
}

export default BaseControl;
