import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
class Control extends BaseControl {
    constructor(props) {
        super("textarea", "多行文本",props?.lock || false);
        this.dataType = 'string';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '多行文本',
            enName: '', // 英文名称
            inputType: 'text', //类型
            formatVerify: 'text', // 格式验证
            limit: 'no_limit', // 限制范围
            minlength: 1, // 最小值
            maxlength: 50, // 最大值
            isSearch: '0', // 智能搜索
            defaultValue: '',
            placeholder: '',
            encryption: false,//是否加密显示
            required: false,
            requiredMessage: '必填字段',
            pattern: '',
            patternMessage: '格式不正确',
            disabled: false,
            readonly: false,
            showWordLimit: false,
            rows: 3,
            autosize: false,
            remark: '',//描述信息
            ...props
        };
        this.rules = [
            { message: '必填字段', required: false },
            { pattern: undefined, message: '格式不正确' }];
    }
}
Control.type = "textarea";
Control.label = "多行文本";
export default { Control, Renderer, PropEditor, Viewer };
