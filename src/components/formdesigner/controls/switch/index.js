import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
class Control extends BaseControl {
    constructor() {
        super("switch", "开关");
        this.dataType='bool';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '开关',
            enName: '',//英文名称
            defaultValue: false,
            required: false,
            requiredMessage: '必填字段',
            disabled: false,
            activeText: '',
            inactiveText: '',
            inlinePrompt: false,
            activeColor: '#409EFF',
            inactiveColor: '#C0CCDA',
            remark: '',//描述信息
        };
        this.rules = [{ required: false }];
    }
    clone() {
        return new Control();
    }
}
Control.type = "switch";
Control.label = "开关";
export default { Control, Renderer, PropEditor,Viewer };
