import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";

class Control extends BaseControl {
    constructor() {
        super("text", "文本");
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '文本',
            fontSize: '12px', // 字体大小
            fontType: 'SimSun', // 字体类型
            fontBold: '0', // 文本加粗
            fontColor: '#000', // 字体颜色
            content: '显示的文本',
            remark: '',//描述信息
        };
    }
}

Control.type = "text";
Control.label = "文本";
export default {Control, Renderer, PropEditor, Viewer: Renderer};
