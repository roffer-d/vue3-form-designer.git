import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';

class Control extends BaseControl {
    constructor(props) {
        super("input", "单行文本", props?.lock || false);
        this.dataType = 'string';
        this.props = {
            type: 'text',
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '单行文本',
            enName: '', // 英文名称
            inputType: 'text', //类型
            formatVerify: 'text', // 格式验证
            defaultValue: '',
            placeholder: '',
            encryption: false,//是否加密显示
            required: false,
            requiredMessage: '必填字段',
            pattern: '',
            patternMessage: '格式不正确',
            disabled: false,
            clearable: false,
            readonly: false,
            isSearch: false,
            showPassword: false,
            showWordLimit: false,
            remark: '',//说明
            minValue:'',//最小值
            maxValue:'',//最大值
            minLength:'',//最小长度
            maxLength:'',//最大长度
            ...props
        };
        this.rules = [
            {message: '必填字段', required: !!props?.required},
            {pattern: undefined, message: '格式不正确'}];
    }
}

Control.type = "input";
Control.label = "单行文本";
export default {Control, Renderer, PropEditor, Viewer};
