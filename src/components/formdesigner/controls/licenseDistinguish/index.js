import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from '../input'
import {remoteOption} from '../../remoteData'

class Control extends BaseControl {
    constructor() {
        super("licenseDistinguish", "许可证识别");
        let nameList = ['许可证号', '企业名称', '企业类型', '负责人姓名', '经营场所', '许可范围', '供货单位', '有限期限', '发证机关', '制证机关']
        let controls = nameList.map(label => new Input.Control({label, required: true, disabled: true, lock: true}))
        this.props = {
            type: 'licenseDistinguish',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '许可证识别',
            enName: '',//许可证识别
            remark: '',//描述信息
            columns: [
                {
                    label: '许可证信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "licenseDistinguish";
Control.label = '许可证识别';
export default {Control, Renderer, PropEditor, Viewer};
