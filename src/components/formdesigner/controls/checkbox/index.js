import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
class Control extends BaseControl {
    constructor() {
        super("checkbox", "多选框组");
        this.dataType='array';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '多选框组',
            enName: '', // 英文名称
            inputType: 'text', //类型
            formatVerify: 'text', // 格式验证
            dictionary: '0', // 匹配字典
            selectLimit: '0', // 多选框限制条件
            defaultValue: [],
            required: false,
            requiredMessage: '必填字段',
            disabled: false,
            showOptionLabel: true,
            remark: '',//描述信息
            options: [
                { value: '值1', label: '选项1' },
                { value: '值2', label: '选项2' }
            ]
        };
        this.rules = [{ message: '必填字段', required: false }];
    }
}
Control.type = "checkbox";
Control.label = "多选框组";
export default { Control, Renderer, PropEditor,Viewer };
