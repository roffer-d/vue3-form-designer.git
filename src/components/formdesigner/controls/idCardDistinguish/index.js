import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from "../input";
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("idCardDistinguish", "身份证识别");
        let nameList = ['姓名', '性别', '民族', '出生', '住址', '身份证号', '图片路径']
        let controls = nameList.map(label => new Input.Control({label, required: true, disabled: true, lock: true}))
        this.props = {
            type: 'idCardDistinguish',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '身份证识别',
            enName: '',//英文名称
            remark: '',//描述信息
            columns: [
                {
                    label: '身份证信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "idCardDistinguish";
Control.label = "身份证识别";
export default {Control, Renderer, PropEditor, Viewer};
