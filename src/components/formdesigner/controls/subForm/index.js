import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';

class Control extends BaseControl {
    constructor() {
        super("subForm", "子表单");
        this.props = {
            type: 'subForm',
            width: 12,
            showLabel: false,
            labelWidth: undefined,
            label: '子表单',
            enName: '',//英文名称
            remark: '',//描述信息
            columns: [
                {
                    label: '子表单1',
                    controls: []
                }
            ],
        };
    }
}

Control.type = "subForm";
Control.label = "子表单";
export default {Control, Renderer, PropEditor, Viewer};
