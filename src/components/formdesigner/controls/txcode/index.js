import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from '../input'
import Select from '../select'
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("txcode", "条形码识别");
        let nameList = ['条形码信息']
        let controls = nameList.map(label => new Input.Control({label, required: true, lock: true}))
        let pinpai = new Select.Control({label: '品牌', required: true, disabled: true, lock: true})
        controls.push(pinpai)

        this.props = {
            type: 'txcode',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '条形码识别',
            enName: '',//条形码识别
            remark: '',//描述信息
            columns: [
                {
                    label: '条形码信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "txcode";
Control.label = '条形码识别';
export default {Control, Renderer, PropEditor, Viewer};
