import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Textarea from "../textarea";

class Control extends BaseControl {
    constructor() {
        super("voiceToText", "语音转文字");
        let nameList = ['文字内容']
        let controls = nameList.map(label => new Textarea.Control({label, required: true, lock: true}))
        this.props = {
            type: 'voiceToText',
            width: 12,
            showLabel: false,
            required: false,
            labelWidth: undefined,
            label: '语音转文字',
            enName: '',//英文名称
            remark: '',//描述信息
            columns: [
                {
                    label: '文字信息',
                    controls
                }
            ],
        };
    }
}

Control.type = "voiceToText";
Control.label = "语音转文字";
export default {Control, Renderer, PropEditor, Viewer};
