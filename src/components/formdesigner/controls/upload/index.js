import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';

class Control extends BaseControl {
    constructor(props) {
        super("upload", "文件上传", props?.lock || false);
        this.dataType = 'string';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '文件上传',
            enName: '',//英文名称
            defaultValue: [],
            buttonText: '点击上传',
            sizeUnit: 'MB',
            size: 5,
            required: false,
            requiredMessage: '必填字段',
            multiple: false,
            withCredentials: false,
            showFileList: true,
            accept: '.jpeg,.jpg,.png',
            limit: 2,
            disabled: false,
            tip: '请上传文件',
            remark: '',//描述信息
            ...props
        };
        this.rules = [{message: '必填字段', required: false}];
    }
}

Control.type = "upload";
Control.label = "文件上传";
export default {Control, Renderer, PropEditor, Viewer};
