import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';

class Control extends BaseControl {
    constructor() {
        super("autograph", "签名");
        this.props = {
            type: 'autograph',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '签名',
            enName: '',//英文名称
            remark: '',//描述信息
        };
    }
}

Control.type = "autograph";
Control.label = "签名";
export default {Control, Renderer, PropEditor, Viewer};
