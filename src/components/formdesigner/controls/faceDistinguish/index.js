import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from "../input";
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("faceDistinguish", "面单识别");
        let nameList = ['快递单号', '寄件公司', '收件人', '收件电话', '收件地址', '寄件人', '寄件电话', '寄件地址', '图片路径']
        let controls = nameList.map(label => new Input.Control({label, required: true, disabled: true, lock: true}))
        this.props = {
            type: 'faceDistinguish',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '面单识别',
            enName: '',//英文名称
            remark: '',//描述信息
            columns: [
                {
                    label: '面单信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "faceDistinguish";
Control.label = "面单识别";
export default {Control, Renderer, PropEditor, Viewer};
