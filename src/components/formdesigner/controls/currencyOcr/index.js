import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from "../input";
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("currencyOcr", "通用OCR");
        let nameList = ['识别文字', '图片路径']
        let controls = nameList.map(label => new Input.Control({label, required: true, disabled: true, lock: true}))
        this.props = {
            type: 'currencyOcr',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '通用OCR',
            enName: '',//英文名称
            remark: '',//描述信息
            columns: [
                {
                    label: '通用OCR信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "currencyOcr";
Control.label = "通用OCR";
export default {Control, Renderer, PropEditor, Viewer};
