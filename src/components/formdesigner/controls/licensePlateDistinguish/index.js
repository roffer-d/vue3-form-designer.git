import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from "../input";
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("licensePlateDistinguish", "车牌识别");
        let nameList = ['车牌号码', '图片路径']
        let controls = nameList.map(label => new Input.Control({label, required: true, disabled: true, lock: true}))
        this.props = {
            type: 'licensePlateDistinguish',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '车牌识别',
            enName: '',//英文名称
            remark: '',//描述信息
            columns: [
                {
                    label: '车牌信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "licensePlateDistinguish";
Control.label = "车牌识别";
export default {Control, Renderer, PropEditor, Viewer};
