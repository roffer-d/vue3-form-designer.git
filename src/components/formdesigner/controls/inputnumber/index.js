import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
class Control extends BaseControl {
    constructor() {
        super("inputnumber", "数字框");
        this.dataType='number';
        this.props = {
            width: 12,
            showLabel: true,
            labelWidth: undefined,
            label: '数字框',
            enName: '', // 英文名称
            defaultValue: 0,
            placeholder: '',
            disabled: false,
            required: false,
            requiredMessage: '必填字段',
            min: 0,
            max: 100,
            step: 1,
            stepStrictly: false,
            precision: 0,
            controls: true,
            controlsPosition: '',
            remark: '',//描述信息
        };
    }
}
Control.type = "inputnumber";
Control.label = "数字框";
export default { Control, Renderer, PropEditor,Viewer };
