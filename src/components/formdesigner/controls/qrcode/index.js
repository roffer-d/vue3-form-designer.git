import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from '../input'
import {remoteOption} from "../../remoteData";

class Control extends BaseControl {
    constructor() {
        super("qrcode", "二维码识别");
        let nameList = ['二维码信息']
        let controls = nameList.map(label => new Input.Control({label, required: true, disabled: true, lock: true}))
        this.props = {
            type: 'qrcode',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '二维码识别',
            enName: '',//二维码识别
            remark: '',//描述信息
            columns: [
                {
                    label: '二维码信息',
                    controls
                }
            ],
            remoteOption: {
                ...remoteOption,
                fieldsMap: nameList.reduce((p, c) => {
                    p[c] = '';
                    return p
                }, {})
            }
        };
    }
}

Control.type = "qrcode";
Control.label = '二维码识别';
export default {Control, Renderer, PropEditor, Viewer};
