import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
import Input from "../input";
import Textarea from "../textarea";

class Control extends BaseControl {
    constructor() {
        super("position", "定位");
        let nameList = ['经度', '纬度']
        let controls = nameList.map(label => new Input.Control({label, required: false}))
        controls.push(new Textarea.Control({label: '地址', required: false}))
        this.props = {
            type: 'position',
            width: 12,
            showLabel: false,
            required: false,
            requiredMessage: '必填字段',
            isEdit: true,//位置是否可编辑
            labelWidth: undefined,
            label: '定位',
            enName: '',//英文名称
            remark: '',//描述信息
            columns: [
                {
                    label: '位置信息',
                    controls
                }
            ],
        };
    }
}

Control.type = "position";
Control.label = "定位";
export default {Control, Renderer, PropEditor, Viewer};
