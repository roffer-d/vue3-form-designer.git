import BaseControl from "../BaseControl";
import Renderer from "./Renderer.vue";
import PropEditor from "./PropsEditor.vue";
import Viewer from './Viewer.vue';
class Control extends BaseControl {
    constructor() {
        super("geographicalPosition", "地理位置");
        this.props = {
            type: 'geographicalPosition',
            width: 12,
            showLabel:false,
            required:false,
            requiredMessage: '必填字段',
            labelWidth: undefined,
            label: '地理位置',
            enName: '',//英文名称
            remark: '',//描述信息
        };
    }
}
Control.type = "geographicalPosition";
Control.label = "地理位置";
export default { Control, Renderer, PropEditor,Viewer };
